import { Component, OnInit } from '@angular/core';
import { AppService } from '../app.service';
import { RegisterService } from '../register.service';

@Component({
  selector: 'login-form',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  providers: [AppService],
})
export class LoginComponent implements OnInit {

  registrationMessage: string = '';
  public loginData = { username: "", password: "" };
  public registerData: any = {
    username: "",
    email: "",
    password: ""
  };

  constructor(private _service: AppService, private registerService: RegisterService) { }

  login() {
    this._service.obtainAccessToken(this.loginData);
  }

  register() {
    this.registerService.register(this.registerData).subscribe(
      rez => {
        console.log('successfully registered: ', rez);
        if (rez.operationSuccessful) {
          this.registerData.username = '';
          this.registerData.email = '';
          this.registerData.password = '';
        }
        this.registrationMessage = rez.operationMessage;
      },
      err => {
        console.log('failure in registering: ', err);
      }
    );
  }

  ngOnInit() {
  }

}



import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Http, RequestOptions, Headers } from '@angular/http';
import { Observable } from 'rxjs';

import { environment } from './../environments/environment';


export class Foo {
  constructor(
    public id: number,
    public email: string) { }
} 
 
@Injectable({
  providedIn: 'root'
})
export class AppService {
  constructor(
    private _router: Router, private _http: Http){}
  
  obtainAccessToken(loginData){
    let params = new URLSearchParams();
    params.append('username',loginData.username);
    params.append('password',loginData.password);    
    params.append('grant_type','password');
    params.append('client_id','smartfarm-client');
    let headers = new Headers({'Content-type': 'application/x-www-form-urlencoded; charset=utf-8',
      'Authorization': 'Basic '+btoa("smartfarm-client:smartfarm-password-client")});
    let options = new RequestOptions({ headers: headers });
     
    this._http.post(environment.contextPathServer + '/oauth/token', 
      params.toString(), options)
      .subscribe(
        rez=> this.saveToken(rez.json()),
        err => {
          console.log('AUTHENTICATION ERROR: ', err);
        }
      );

  }
 
  saveToken(token){
    var expireDate = new Date().getTime() + (1000 * token.expires_in);
    // Cookie.set("access_token", token.access_token, expireDate);
    console.log('TOKEN FROM SERVER: ', token);
    localStorage.setItem("access_token", token.access_token);
    localStorage.setItem('loggedin', 'yes');
    this._router.navigate(['/']);
    // location.reload();
  }
 
  getResource(resourceUrl)  {
    var headers = new Headers({'Content-type': 'application/x-www-form-urlencoded; charset=utf-8',
      'Authorization': 'Bearer '+localStorage.getItem('access_token')});
    var options = new RequestOptions({ headers: headers });
                       // .map((res:Response) => res.json())

                   //.catch((error:any) => Observable.throw(error.json().error || 'Server error'));
    return this._http.get(resourceUrl, options)

                   .subscribe(
                     rez => {
                       console.log('Got resource from REST smartfarm server: ', rez)
                     },
                     err => {
                       console.log('COULD NOT LOAD RESOURCE')
                     }
                   );
  }
 
  checkCredentials(){
    if (!localStorage.getItem('access_token')){
        this._router.navigate(['/login']);
    }
  } 
 
  logout() {
    localStorage.removeItem('access_token');
    this._router.navigate(['/login']);
  }
}
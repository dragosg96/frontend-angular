import { TestBed, inject } from '@angular/core/testing';

import { IrigationService } from './irigation.service';

describe('IrigationService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [IrigationService]
    });
  });

  it('should be created', inject([IrigationService], (service: IrigationService) => {
    expect(service).toBeTruthy();
  }));
});

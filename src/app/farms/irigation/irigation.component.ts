import { Component, OnInit, AfterViewInit } from '@angular/core';
import { IrigationService } from '../irigation.service';
import { ActivatedRoute } from '@angular/router';
import { MapLoaderService } from '../map.loader'
import { LogoutService } from '../../logout.service';
declare var google: any;

@Component({
  selector: 'app-irigation',
  templateUrl: './irigation.component.html',
  styleUrls: ['./irigation.component.css']
})
export class IrigationComponent implements OnInit, AfterViewInit {

  irigationOperationId: number;
  irigationOperation: any;
  sectorProperties: any[] = [];
  sectorPropertyTypes: string[] = []; // umiditate, temperatura etc.
  sectorPropertyTypeSelected: string = '';
  initialSectorProperties: any[] = [];

  // google maps
  drawingManager: any;
  map: any;

  constructor(private irigationService: IrigationService, private activatedRoute: ActivatedRoute,
  private logoutService : LogoutService) { }

  ngOnInit() {
    this.activatedRoute.params.subscribe(params => {
      this.irigationOperationId = params['id'];

      this.irigationService.findOperationPropertyById(this.irigationOperationId).subscribe(
        rez => {
          this.irigationOperation = rez.json();
          console.log('Irigation operation loaded: ', this.irigationOperation);

          let dateApplied = this.irigationOperation.farmOperationProperty.dateApplied;
          console.log('date Applied = = = ', dateApplied);
          let dateElements = ('' + dateApplied).split('-');
          let year = parseInt(dateElements[0]);
          let month = parseInt(dateElements[1]);
          let day = parseInt(dateElements[2]);
          this.irigationService.findSectorProperties(year, month, day).subscribe(
            rez => {
              this.sectorProperties = rez.json();
              this.initialSectorProperties = [...rez.json()];
              let uniquePropertyTypes = new Set();
              console.log('sector properties for date: ', this.sectorProperties);
              for (let sp of this.sectorProperties) {
                uniquePropertyTypes.add(sp.propName);
              }
              this.sectorPropertyTypes = Array.from(uniquePropertyTypes);
              console.log('SECTOR PROP TYPES = ', this.sectorPropertyTypes)
            },
            err => {
              console.log('could not load sector properties, err = ', err);
            }
          );
         
        },
        err => {
          console.log('could not load operation property', err);
          console.log('ERROR STATUS: ', err.status);
          if (err.status == 401) { 

            this.logoutService.openDialog();


          }
        }
      );
    });
  }


  ngAfterViewInit() {
    console.log('after view init sector map component')
    MapLoaderService.load().then(() => {
      this.drawPolygon();
    })
  }


  onChange(sectorPropertyType) {

    let spname = sectorPropertyType.value.split(":")[1].trim();
    let filteredSectorProperties = this.initialSectorProperties.filter(x => { return x.propName == spname });
    this.sectorProperties = filteredSectorProperties;


  }


  drawCoordinates(coordinatesObject, map) {
    console.log('drawing: ', coordinatesObject);


    var sectorPolygon = new google.maps.Polygon({
      paths: coordinatesObject,
      strokeColor: '#FF0000',
      strokeOpacity: 0.8,
      strokeWeight: 2,
      fillColor: '#FF0000',
      fillOpacity: 0.35,
      editable: true
    });
    sectorPolygon.setMap(map);




  }

  drawPolygon() {
    // TODO: center the map to the specific area in which the farm sector is located
    this.map = new google.maps.Map(document.getElementById('map'), {
      center: { lat: -34.397, lng: 150.644 },
      zoom: 8
    });

    this.drawingManager = new google.maps.drawing.DrawingManager({
      drawingMode: google.maps.drawing.OverlayType.POLYGON,
      drawingControl: true,
      drawingControlOptions: {
        position: google.maps.ControlPosition.TOP_CENTER,
        drawingModes: ['polygon']
      }
    });

    this.drawingManager.setMap(this.map);

    this.drawCoordinates(this.irigationOperation.dtoPolygon.coordinates, this.map);
  }

}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IrigationComponent } from './irigation.component';

describe('IrigationComponent', () => {
  let component: IrigationComponent;
  let fixture: ComponentFixture<IrigationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IrigationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IrigationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

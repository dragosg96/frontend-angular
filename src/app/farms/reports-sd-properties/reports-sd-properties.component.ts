import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { FarmsrvService } from '../farmsrv.service';
import { group } from '@angular/animations';
import { LogoutService } from '../../logout.service';

@Component({
  selector: 'app-reports-sd-properties',
  templateUrl: './reports-sd-properties.component.html',
  styleUrls: ['./reports-sd-properties.component.css']
})
export class ReportsSdPropertiesComponent implements OnInit, OnDestroy {


  ngOnDestroy(): void {
    console.log('stopping request interval')
    clearInterval(this.refreshSectorPropertyInterval)
  }

  idSector: number;
  sector: any;
  refreshSectorPropertyInterval: any = null;
  propertyTypes = []; 
  selectedReportType : string = '';
  reportError : boolean = false;

  chartData = [];
  chartColumnNames = [];

  constructor(private activatedRouterService: ActivatedRoute,
    private service: FarmsrvService, private logoutService : LogoutService) { }

  ngOnInit() {
    this.activatedRouterService.params.subscribe(params => {
      this.idSector = params['idsector'];



      this.service.findSectorById(this.idSector).subscribe(rez => {
        this.sector = rez.json(); // load sector
        this.initReport();
        this.startInterval();
      },
        err => {
          console.log('error loading farm sector ')
          console.log('ERROR STATUS: ', err.status);
          if (err.status == 401) {

            this.logoutService.openDialog();


          }
        });

    });
  }


  startInterval() {
    this.refreshSectorPropertyInterval = setInterval(() => {
      this.refreshAllSectorProperties();
      this.generateReport(); // refrehs report
    }, 3000);
  }

  isReportDrawn : boolean = false;

  initReport(){
    let propertyNames = new Set<string>();
    let propertyNamesArray =  [];
    for (let prop of this.sector.farmSectorProperties){
      propertyNames.add(prop.propName);
      
    }
    
    
    propertyNames.forEach(x => {propertyNamesArray.push(x)})

    for(let pn of propertyNamesArray){
      this.propertyTypes.push(pn);
      
    }
    console.log('Property types array = ', this.propertyTypes)
  }

  generateReport(){

    if(this.selectedReportType == ''){
      this.reportError = true;
      return;
    }
    this.reportError = false;
    
    let propertyNames = new Set<string>();
    let propertyNamesArray =  [];
    for (let prop of this.sector.farmSectorProperties){
      propertyNames.add(prop.propName);
      
    }
    
    
    propertyNames.forEach(x => {propertyNamesArray.push(x)})

    let groupedProperties = {};
    for(let pn of propertyNamesArray){
      groupedProperties[pn] = [];
      
    }

    console.log('SECTOR PROPERTIES FROM SERVER: ', this.sector.farmSectorProperties)
    this.sector.farmSectorProperties.map(x => {
      groupedProperties[x.propName].push(x);
    });
    console.log("Grouped properties: ", groupedProperties);
    this.chartData = this.generateChart(groupedProperties, this.selectedReportType);
    this.isReportDrawn = true;
    console.log('date umiditate test: ', this.chartData);
  }

  generateChart(groupedProperties, propertyName) : any[]{
    let chartData = [];
    let DATE_FORMAT_PATTERN = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";

    for(let entry of groupedProperties[propertyName]){
      let dateOk =  new Date(entry.dateMeasured);
      console.log('DATE OKAY: ', dateOk.getTime());
      
      chartData.push([dateOk.getTime(),  parseInt(entry.propValue)]);

    }
    

    this.chartColumnNames = ['date measured', propertyName];

    function Comparator(a, b) {
     
      return a[0] - b[0];
    }
   
    
   
    chartData = chartData.sort(Comparator);
    chartData = chartData.map(x => {
      console.log('element x = ', x);
      let dateElement = new Date(x[0]);
      x[0] = `${dateElement.getFullYear()}-${dateElement.getMonth()}-${dateElement.getUTCDate()}`;
      return x;
    });
    
    console.log('chart Data - ', chartData);

    return chartData;
  }

  refreshAllSectorProperties() {
    this.service.findAllSectorProperties(this.sector.id).subscribe(
      rez => {
        this.sector.farmSectorProperties = rez.json();
        for (let fsp of this.sector.farmSectorProperties) {
          fsp.dateMeasured = this.formatDate(fsp.dateMeasured);
        }
        console.log('SECTOR PROPERTIES FOR REPORT: ', this.sector.farmSectorProperties);
      },
      err => {
        console.log('could not load properties for current sector: ', err);
      }
    );
  }

  formatDate(date) {
    var d = new Date(date),
      month = '' + (d.getMonth() + 1),
      day = '' + d.getDate(),
      year = d.getFullYear();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;

    return [year, month, day].join('-');
  }



}

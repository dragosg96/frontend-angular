import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReportsSdPropertiesComponent } from './reports-sd-properties.component';

describe('ReportsSdPropertiesComponent', () => {
  let component: ReportsSdPropertiesComponent;
  let fixture: ComponentFixture<ReportsSdPropertiesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReportsSdPropertiesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReportsSdPropertiesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

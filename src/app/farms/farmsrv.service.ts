import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs';
import { environment} from './../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class FarmsrvService {

  private endpointUrl : string = environment.contextPathServer + '/rest/farms';
  private contextPath : string = environment.contextPathServer + '/rest';
  private endpointUrlAddress : string = environment.contextPathServer + '/rest/addresses';

  constructor(private _http : Http) { }

  appendToken(){
    return "?access_token="+localStorage.getItem('access_token');
  }

  findAllFarms() {
    return this._http.get(this.endpointUrl+"/farm/all" + this.appendToken());
  }


  findFarmById(idfarm : number){
    return this._http.get(this.endpointUrl+"/farm/byid/"+idfarm  + this.appendToken());
  }

  deleteFarmById(idfarm : number){
    return this._http.delete(this.endpointUrl+"/farm/delete/"+idfarm  + this.appendToken());
  }
  findAllOperations(){
    return this._http.get(this.contextPath+"/operations/all"  + this.appendToken());
  }

  findAllSectors(){
    return this._http.get(this.endpointUrl+"/sectors/all"  + this.appendToken());
  }

  findSectorById(id : Number){
    
    return this._http.get(this.contextPath + "/farms/sectors/byid/"+id  + this.appendToken());
  }

  findAllSectorsForFarm(farmId : Number){
    return this._http.get(this.endpointUrl + "/sectors/byfarmid/"+farmId  + this.appendToken());
  }


  findAllSectorProperties(sectorId : number){
    return this._http.get(this.endpointUrl + "/sectors/properties/find/" + sectorId  + this.appendToken());
  }


  findAllSoilTypes(){
    return this._http.get(this.contextPath + "/soils/all" + this.appendToken());
  }


  saveFarm(farmJson){
    return this._http.post(this.endpointUrl + "/save" + this.appendToken(), farmJson);
  }

  saveNewSectorForFarm(farmId : Number, farmSector){
    return this._http.post(this.endpointUrl +"/sectors/add/"+farmId  + this.appendToken(), farmSector);
  }

  saveFarmSectorDetail(farmSectorId : Number, farmSectorDetail){
    return this._http.post(this.endpointUrl +"/sectors/details/add/"+farmSectorId  + this.appendToken(), farmSectorDetail);
  }

  saveFarmOperationPropertyToFarmSector(farmSectorId : Number, farmSectorDetailId : Number, propertyName : string, propertyValue : string, farmOperation : any){
    return this._http.post(this.endpointUrl + "/sectors/details/operations/"+farmSectorId+"/" + farmSectorDetailId + "/"+propertyName+"/"+propertyValue  + this.appendToken(), farmOperation);
  }

  saveFarmSectorProperty(farmSectorPropertyDTO : any){
    return this._http.post(this.endpointUrl + "/sectors/properties/add"  + this.appendToken(), farmSectorPropertyDTO);
  }


  deleteFarmSector(farmSectorId : Number){
    return this._http.delete(this.endpointUrl +"/sectors/remove/"+farmSectorId  + this.appendToken());
  }

  deleteSectorProperty(farmSectorPropertyId : Number){
    return this._http.delete(this.endpointUrl +"/sectors/remove/property/"+farmSectorPropertyId  + this.appendToken());

  }

  deleteSectorDetail(sectorDetailId : Number){
    return this._http.delete(this.endpointUrl +"/sectors/remove/detail/"+sectorDetailId  + this.appendToken());
  }
  deleteOperationDetail(opDetail : Number, idsectordetail : Number){
    return this._http.delete(this.endpointUrl + "/sectors/details/operations/delete/"+opDetail+"/"+idsectordetail  + this.appendToken());
  }

  unbindOperationDetail(opDetail : Number, idsectordetail : Number){
    return this._http.delete(this.endpointUrl + "/sectors/details/operations/unbind/"+opDetail+"/"+idsectordetail  + this.appendToken());
  }
  updateProperty(sectorid : Number, property : any){
    property.farmSectorId = sectorid;
    // private Integer farmSectorPropertyId; // for update
    // private Integer farmSectorId;
    // private String propertyName;
    // private String propertyValue;

    // {id: 1, propName: "humidity 2", propValue: "300"}
    let jsonProperty = {
      farmSectorPropertyId : property.id,
      farmSectorId : sectorid,
      propertyName : property.propName,
      propertyValue : property.propValue,
      dateMeasured : property.dateMeasured
    };
    return this._http.put(this.endpointUrl + "/sectors/properties/update"  + this.appendToken(), jsonProperty);
  }

  updateSectorDetails(sectorId, sectorDetail){
    
    return this._http.put(this.endpointUrl + `/sectors/sectordetails/${sectorId}${this.appendToken()}`, sectorDetail);
  }

  updateSectorCoordinates(sectorId, sectorCoordinates){
    return this._http.put(this.contextPath + `/geo/sector/update/${sectorId}${this.appendToken()}`, sectorCoordinates);
  }

  updateSectorCoordinatesPolygon(sectorId, sector){
    // /rest/farms/sectors/byid/1
    return this._http.put(this.contextPath + `/farms/sectors/update/${sectorId}${this.appendToken()}`, sector);
  }

  updateSectorDeleteCoordinates(sectorId){
    // sectors/updatecoordnull/{idsector}
    return this._http.put(this.contextPath + `/farms/sectors/updatecoordnull/${sectorId}${this.appendToken()}`, {});
  }

  updateAddress(address){
    return this._http.put(this.endpointUrlAddress + '/update' + this.appendToken(), address);
  }

  saveAddress(address, fermaId){
    return this._http.post(this.endpointUrlAddress+'/save/'+fermaId + this.appendToken(), address);
  }

  // TODO: create geolocation service
  getLatLongFromAddress(address){
    var API_KEY = 'b610d05a792943d3aa4ca4b95076f48e';

    let url = `https://api.opencagedata.com/geocode/v1/json?q=${address.street}%2C%20${address.city}&key=${API_KEY}&language=ro&pretty=1`;
    return this._http.get(url);

  }

}

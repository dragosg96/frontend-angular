import { TestBed, inject } from '@angular/core/testing';

import { FarmsrvService } from './farmsrv.service';

describe('FarmsrvService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [FarmsrvService]
    });
  });

  it('should be created', inject([FarmsrvService], (service: FarmsrvService) => {
    expect(service).toBeTruthy();
  }));
});

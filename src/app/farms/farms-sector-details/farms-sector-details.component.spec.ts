import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FarmsSectorDetailsComponent } from './farms-sector-details.component';

describe('FarmsSectorDetailsComponent', () => {
  let component: FarmsSectorDetailsComponent;
  let fixture: ComponentFixture<FarmsSectorDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FarmsSectorDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FarmsSectorDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

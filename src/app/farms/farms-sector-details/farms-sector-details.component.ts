import { Component, OnInit, Input, Output, EventEmitter, AfterViewInit, OnChanges, SimpleChanges } from '@angular/core';
import { FarmsrvService } from '../farmsrv.service';


import { MAT_DATE_LOCALE } from '@angular/material';
import { LogoutService } from '../../logout.service';
import { SectorintervalService } from '../sectorinterval.service';







export const YYYY_MM_DD_Format = {
  parse: {
    dateInput: 'DD-MM-YYYY',

  },

  display: {
    dateInput: 'DD-MM-YYYY',
    monthYearLabel: 'MMM YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM YYYY',

  },

};


@Component({
  selector: 'app-farms-sector-details',
  templateUrl: './farms-sector-details.component.html',
  styleUrls: ['./farms-sector-details.component.css'],
  providers: [

    { provide: MAT_DATE_LOCALE, useValue: YYYY_MM_DD_Format }

  ]
})
export class FarmsSectorDetailsComponent implements OnInit, OnChanges {


  public ipp: number = 5;

  lastOperationPropertyByCategory: any = {

  };
  lastOperationPropertyByCategoryArray: any[] = [];

  shouldChooseTime: boolean;

  selectedHour: number;
  selectedMinute: number;

  intervalDtos: any[] = [];

  @Input() sector: any;
  @Output() evenimentSectorDetailsComponent: EventEmitter<string> = new EventEmitter();
  // @Output() eveniment : EventEmitter<string> = new EventEmitter();

  ngOnChanges(changes: SimpleChanges): void {
    console.log('FARM SECTOR DETAILS COMPONENT CHANGE')
  }



  currentPage: number = 1;

  operations: any[] = [];
  operationIdAndName: any = {};

  constructor(private serviceFarm: FarmsrvService, private logoutService: LogoutService,
    private intervalService: SectorintervalService) { }

  ngOnInit() {

    
    this.intervalService.findIntervalsForSector(this.sector.id).subscribe(
      rez => {
        this.intervalDtos = rez;
        console.log('successfully loaded intervals: ', this.intervalDtos);
        this.serviceFarm.findAllOperations().subscribe(
          rez => {
            this.operations = rez.json();
            console.log('EXISTING OPERATIONS: ', this.operations);
            
            for (let operationType of this.operations) {
              this.lastOperationPropertyByCategory[operationType.id] = []
              this.operationIdAndName[operationType.id] = operationType.operationName;
            }

            // populate lastOperationByCategory array with the values from the already loaded sector
            for (let fsd of this.sector.farmSectorDetails) {

              if (fsd.farmOperationProperty) {
                let operationId = fsd.farmOperationProperty.farmOperation.id;
                this.lastOperationPropertyByCategory[operationId].push(fsd.farmOperationProperty);
              }
            }

            for (let idCategory of Object.keys(this.lastOperationPropertyByCategory)) {
              let lastOperationCategory = this.lastOperationPropertyByCategory[idCategory];

              for (let opProp of lastOperationCategory) {
                opProp.dateApplied = new Date(opProp.dateApplied);
                console.log('DATE APPLIED TEST: ', opProp.dateApplied.getTime());
              }
              lastOperationCategory = lastOperationCategory.sort((x, y) => y.dateApplied.getTime() - x.dateApplied.getTime());
            }

            // lastOperationPropertyByCategory contains all the operation properties sorted by date (desc)
            // only the firt element of the array (should it exist), should be added
            // in order to compare with the interval
            for (let idOpCat of Object.keys(this.lastOperationPropertyByCategory)) {
              let opCat = this.lastOperationPropertyByCategory[idOpCat];
              console.log('OP CAT: ', opCat);
              if (opCat.length > 0) {

                this.lastOperationPropertyByCategory[idOpCat] = opCat[0];
                let now = new Date();
                let dateApplied = opCat[0].dateApplied;
                // console.log('DIFFERENCE: ', (now.getTime() - dateApplied.getTime()));
                let diff = now.getTime() - dateApplied.getTime();
                // console.log('diff for operation:  ' + opCat[0] + ' is : ' + diff);
                if(diff < 0){
                  opCat[0].expirationTimer = null;  
                }else{
                  let correspondingInterval = null;
                  for(let intervalS of this.intervalDtos){
                    if(intervalS.operation.id == opCat[0].farmOperation.id){
                      correspondingInterval = intervalS;
                      opCat[0].correspondingInterval = correspondingInterval;
                      break;
                    }
                  }
                  if(correspondingInterval != null){
                    opCat[0].expirationTimer = diff / (1000 * 60); 
                  }else{
                    opCat[0].expirationTimer = null; // operations which do not have an associated interval (e.g. seeding)
                  }
                  
                }

                
                this.lastOperationPropertyByCategoryArray.push(opCat[0]);

              } else {
                this.lastOperationPropertyByCategory[idOpCat] = null;
              }
            }

            console.log('LAST OPERATION ARRAY: ', this.lastOperationPropertyByCategoryArray);
          },
          err => {

            console.log('ERROR STATUS: ', err.status);
            if (err.status == 401) { 
              this.logoutService.openDialog();
            }
          }
        );
      },
      err => {
        console.log('could not load intervals', err)
      }
    );



  }



  addSectorDetail(sector) {


    let applicationDate = sector.applicationDate;
    let propertyName = sector.propertyName;
    let propertyValue = sector.propertyValue;
    let farmOperation = sector.selectedOperation;



    if (this.shouldChooseTime) {
      applicationDate.setHours(this.selectedHour, this.selectedMinute, 0);
    } else {
      applicationDate.setHours(0, 0, 0);
    }
 

    let json = {
      applicationDate: applicationDate,
      farmOperationProperty: {
        propertyName: propertyName,
        propertyValue: propertyValue,
        farmOperation: farmOperation
      }
    };
    this.serviceFarm.saveFarmSectorDetail(sector.id, json).subscribe(
      rez => {
        console.log('salvarea farm sector detail fu ok');
        console.log(rez.json());

        this.evenimentSectorDetailsComponent.emit('ADDED SECTOR DETAIL (operation included)');
      },
      err => {
        console.log('eroare salvare farm sector detail', err);
      }
    );

  }

  /**
    * 
    * @param sectorId 
    * @param sd sectorDetails object to update
    */
  updateSectorDetails(sectorId, sd) {
    console.log('***sector: ' + sectorId);
    console.log('updating sd: ', sd);
    this.serviceFarm.updateSectorDetails(sectorId, sd).subscribe(
      rez => {
        console.log('successfully updated, RESPONSE: ', rez.json());
        this.evenimentSectorDetailsComponent.emit('SECTOR DETAILS - UPDATE');
      },
      err => {
        console.log('failure to update: ', err);
      }
    );
  }




  refreshFarmSectorDetails() {
    this.evenimentSectorDetailsComponent.emit('info child');
  }


  addOPToSectorDetail(farmSectorId, farmSectorDetailId, propertyName, propertyValue, selectedOperation) {
    console.log("PROPERTY NAME: " + propertyName + " PROPERTY VALUE: " + propertyValue + " SELECTED OPERATION: ", selectedOperation);
    this.serviceFarm.saveFarmOperationPropertyToFarmSector(farmSectorId, farmSectorDetailId, propertyName, propertyValue, selectedOperation).subscribe(
      rez => {
        console.log('SAVE OK', rez.json());
        this.evenimentSectorDetailsComponent.emit('SECTOR DETAILS - ADD OP TO SECTOR DETAIL');
      },
      err => {
        console.log('SAVE FAILED', err);
      }
    );
  }



  deleteSectorDetails(detail) {
    this.serviceFarm.deleteSectorDetail(detail.id).subscribe(
      rez => {
        console.log('saved new sector', rez.json());
        // this.sectoare = rez.json();
        //this.refreshFarmSectorDetails();
        this.evenimentSectorDetailsComponent.emit('SECTOR DETAILS - DELETE');
      },
      err => {
        console.log('error: ', err);
      }
    );
  }

  deleteOperationProperty(farmOperationProperty, sectorId, sector) {
    console.log('deleting FOP: ', farmOperationProperty);
    this.serviceFarm.unbindOperationDetail(farmOperationProperty.id, sectorId).subscribe(
      rez => {
        console.log('successful unbound OP', rez.json());
        this.serviceFarm.deleteOperationDetail(farmOperationProperty.id, sectorId).subscribe(
          rezDelete => {
            console.log('successful delete OP', rezDelete.json());
            // this.reloadSectors();
            this.evenimentSectorDetailsComponent.emit('SECTOR DETAILS - DELETE OPERATION PROPERTY FOR SECTOR DETAIL (2)');
            // sector.farmSectorProperties.splice(sector.farmSectorProperties.indexOf(farmOperationProperty), 1);
          },
          errDelete => {
            console.log('error delete OP: ', errDelete);
          }
        );
      },
      err => {
        console.log('error unbind: ', err);
      }
    );
  }

  pageChange(event) {
    console.log('pg number: ', event);
    this.currentPage = event;
  }


}

import { FarmsModule } from './farms.module';

describe('FarmsModule', () => {
  let farmsModule: FarmsModule;

  beforeEach(() => {
    farmsModule = new FarmsModule();
  });

  it('should create an instance', () => {
    expect(farmsModule).toBeTruthy();
  });
});

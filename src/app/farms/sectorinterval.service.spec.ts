import { TestBed, inject } from '@angular/core/testing';

import { SectorintervalService } from './sectorinterval.service';

describe('SectorintervalService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SectorintervalService]
    });
  });

  it('should be created', inject([SectorintervalService], (service: SectorintervalService) => {
    expect(service).toBeTruthy();
  }));
});

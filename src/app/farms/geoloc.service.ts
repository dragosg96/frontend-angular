import { Injectable } from '@angular/core';
import { Http } from '@angular/http';


@Injectable({
  providedIn: 'root'
})
export class GeolocService {

  constructor(private _http : Http) { }


  
  getLatLongFromAddress(address){
    var API_KEY = 'b610d05a792943d3aa4ca4b95076f48e';
    // var address = {
    //     city: "Bucharest",
    //     street: "Zece Mese",
    //     number: "8"
    // };
    // https://api.opencagedata.com/geocode/v1/json?q=Moabit%2C%20Berlin&key=b610d05a792943d3aa4ca4b95076f48e&language=ro&pretty=1
    let url = `https://api.opencagedata.com/geocode/v1/json?q=${address.street}%2C%20${address.city}&key=${API_KEY}&language=ro&pretty=1`;
    console.log('making request to URL: ' + url);

    return this._http.get(url);

    
  }
}

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment} from './../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class SectorintervalService {

  private restIntervalUrl = environment.contextPathServer + '/rest/interval';

  appendToken(){
    return "?access_token="+localStorage.getItem('access_token');
  }

  constructor(private httpClient : HttpClient) { }

  // dtobysectorid/{sectorid}

  findIntervalsForSector(sectorid : number) : Observable<any[]>{
    return this.httpClient.get<any[]>(`${this.restIntervalUrl}/dtobysectorid/${sectorid}`);
  }
}

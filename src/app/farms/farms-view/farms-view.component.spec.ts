import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FarmsViewComponent } from './farms-view.component';

describe('FarmsViewComponent', () => {
  let component: FarmsViewComponent;
  let fixture: ComponentFixture<FarmsViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FarmsViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FarmsViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

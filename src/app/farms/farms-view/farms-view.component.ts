import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FarmsrvService } from '../farmsrv.service';
import { Router } from '@angular/router';
import { LogoutService } from '../../logout.service';

@Component({
  selector: 'app-farms-view',
  templateUrl: './farms-view.component.html',
  styleUrls: ['./farms-view.component.css']
})
export class FarmsViewComponent implements OnInit {

  showProps = false;
  editAddress = false;
  soilTypes : any = [];

  @Input() ferma : any;

  sectoare : any[] = [];
  // the new address
  address : any = {};

  lat: number = 51.678418;
  lng: number = 7.809007;
  newSector : any = {
        
    "sectorDescription": "",
    "surface": 0,
    "soilTypeBean" : null
  
  };


  constructor(private serviceFarm : FarmsrvService, private logoutService : LogoutService) { }



  // refresh sectors for farm
  reloadSectors(){
    this.serviceFarm.findAllSectorsForFarm(this.ferma.id).subscribe(
      rez=>{
        console.log('toate sectoarele pentru ferma curenta: ', rez.json());
        this.sectoare = rez.json();
      },
      err=>{
        console.log('error load farm sectors: ', err);
        console.log('ERROR STATUS: ', err.status);
        if (err.status == 401) {

          this.logoutService.openDialog();


        }
      }
    );
  }



  loadCoordFromAddress(address){


    this.serviceFarm.getLatLongFromAddress(address).subscribe(
      rez=>{
        console.log('lat long: ', rez.json());
        let jsonRez = rez.json();
        
        this.lat = jsonRez.results[0].geometry.lat;
        this.lng = jsonRez.results[0].geometry.lng;
        console.log('LATITUDE: ' + this.lat + ' LONGITUDE: ' + this.lng);
      }, 
      err =>{
        console.log(err);
      }
    );

    

  }

  ngOnInit() {
    this.serviceFarm.findAllSoilTypes().subscribe(
      rez => {
        this.soilTypes = rez.json();
      },
      err => {
        console.log('could not load soil types: ', err);
      }
    );

    console.log('FERMA CURENTA: ', this.ferma);
    
    if(this.ferma.address){
      this.loadCoordFromAddress(this.ferma.address);
    }

    this.reloadSectors();
   
  }

  saveNewSector(event){
    event.preventDefault();
    this.serviceFarm.saveNewSectorForFarm(this.ferma.id, this.newSector).subscribe(
      rez=> {
        console.log('saved new sector', rez.json());
        this.sectoare = rez.json();
      },
      err=>{
        console.log('error: ', err);
      }
    );
  }

  refreshFarmSectorDetails(farmSectorId : Number, newSectors : any[]){
    for(let i=0; i<this.sectoare.length; i++){
      let sectorCurent = this.sectoare[i];
      if(sectorCurent.id == farmSectorId) {
        sectorCurent.farmSectorDetails = newSectors;
      }
    }
  }



  


  delSector(sector){
    this.serviceFarm.deleteFarmSector(sector.id).subscribe(
      rez=> {
        console.log('saved new sector', rez.json());
        this.sectoare = rez.json();
      },
      err=>{
        console.log('error: ', err);
      }
    );
  }

 





  updateAddress(address){
    console.log('updating address: ', address);
    this.serviceFarm.updateAddress(address).subscribe(
      rez => {
        console.log(rez.json());
        alert('Successfully updated address')
        location.reload();

      },
      err => {
        
        alert('could not update address');
        console.log(err);
      }
    );
  }

  saveAddress(address, fermaId){
    console.log('saving address: ', address);
    console.log('ferma id: ' + fermaId);
    this.serviceFarm.saveAddress(address, fermaId).subscribe(
      rez => {
        console.log(rez.json());
        // this.logoutService.openDialog("Address saved", "The address has been successfully saved", "OK");
      },
      err => {
        alert('could not save address');
        console.log(err);
      }
    );
  }

  
}

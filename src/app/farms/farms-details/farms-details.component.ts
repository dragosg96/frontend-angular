import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { FarmsrvService } from '../farmsrv.service';
import { GeolocService } from '../geoloc.service';
import { LogoutService } from '../../logout.service';


@Component({
  selector: 'app-farms-details',
  templateUrl: './farms-details.component.html',
  styleUrls: ['./farms-details.component.css']
})
export class FarmsDetailsComponent implements OnInit {

  currentFarm: any = null;
  currentFarmId: number;
  

  constructor(private route: ActivatedRoute, private service: FarmsrvService, private geoloc: GeolocService,
    private logoutService: LogoutService) { }



  ngOnInit() {

    
    this.route.params.subscribe(params => {
      this.currentFarmId = params['idfarm'];


      this.service.findFarmById(this.currentFarmId).subscribe(
        rez => {
          console.log('loaded all farms: ', rez.json());
          this.currentFarm = rez.json();

          let f = this.currentFarm;

          if (this.currentFarm.address) {
            this.geoloc.getLatLongFromAddress(f.address).subscribe(
              res => {
                console.log('GEOLOC RESULTS: ', res.json());


                let latLong = res.json().results[0].geometry;
                console.log('LATLONG: ', latLong);
                f.coord = latLong;
              },
              err => {
                console.log('GEOLOC ERROR: ', err);
              }
            );
          }


        },
        err => {
          console.log('ERROR STATUS: ', err.status);
          if (err.status == 401) { // initiate mat dialog modal service
            // which will result in the user being redirected to the
            // login component (via router)

            this.logoutService.openDialog();


          }else if (err.status == 403){
            // resource is not accessible - farm does not belong to the currently logged in user
            // alert('You cannot view this farm')
            this.logoutService.openDialog("Private Farm Profile", "You cannot view this farm's details", "OK");
          }
        });


    });
  }



}

import { Component, OnInit, AfterViewInit } from '@angular/core';
import { MapLoaderService } from '../map.loader'

import { ActivatedRoute } from '@angular/router';
import { FarmsrvService } from '../farmsrv.service';
import { LogoutService } from '../../logout.service';
declare var google: any;


@Component({
  selector: 'app-sector-map',
  templateUrl: './sector-map.component.html',
  styleUrls: ['./sector-map.component.css']
})
export class SectorMapComponent implements OnInit, AfterViewInit {

  map: any;
  drawingManager: any;
  sectorId: number;
  selectedShape: any = null;
  allShapes: any[] = [];
  farmSector: any = null;
  isShapeDrawn: boolean = false;


  // bounds of the desired area
  allowedBounds = null;
  lastValidCenter = null; 



  constructor(private route: ActivatedRoute, private service: FarmsrvService,
    private logoutService: LogoutService) { }

  ngOnInit() {

   

    this.route.params.subscribe(params => {
      this.sectorId = params['idsector']; 



      this.service.findSectorById(this.sectorId).subscribe(
        rez => {
          console.log('FOUND farm sector: ', rez.json());
          this.farmSector = rez.json();
          this.drawPolygon();
        },
        err => {
          console.log('error loading farm sector');

          console.log('ERROR STATUS: ', err.status);
          if (err.status == 401) {
            this.logoutService.openDialog();


          }

        }
      );

    });
  }

  ngAfterViewInit() {
    console.log('after view init sector map component')
    MapLoaderService.load().then(() => {
      this.drawPolygon();
      this.allowedBounds = new google.maps.LatLngBounds(
        new google.maps.LatLng(43.623308113, 20.2331701528),
        new google.maps.LatLng(48.2202406655, 29.8352209341)
      );
    })
  }

  drawCoordinates(coordinatesObject, map) {
    console.log('drawing: ', coordinatesObject);


    var sectorPolygon = new google.maps.Polygon({
      paths: coordinatesObject,
      strokeColor: '#FF0000',
      strokeOpacity: 0.8,
      strokeWeight: 2,
      fillColor: '#FF0000',
      fillOpacity: 0.35,
      editable: true
    });
    sectorPolygon.setMap(map);

    google.maps.event.addListener(sectorPolygon, 'click', function () {
      console.log('click on drawn polygon');
    });

    this.attachEventListeners(sectorPolygon);


  }



  drawPolygon() {
    this.map = new google.maps.Map(document.getElementById('map'), {
      center: { lat: 44.439663, lng: 26.096306 },
      zoom: 8
    });

    this.lastValidCenter = this.map.getCenter();
    console.log('Last valid center initial: ', this.lastValidCenter)


    this.drawingManager = new google.maps.drawing.DrawingManager({
      drawingMode: google.maps.drawing.OverlayType.POLYGON,
      drawingControl: true,
      drawingControlOptions: {
        position: google.maps.ControlPosition.TOP_CENTER,
        drawingModes: ['polygon']
      }
    });

    this.drawingManager.setMap(this.map);

    google.maps.event.addListener(this.map, 'center_changed', () => {
      console.log('Are you on map? ', this.allowedBounds.contains(this.map.getCenter()));
      console.log(this.allowedBounds);
      console.log(this.map.getCenter());
      if (this.allowedBounds.contains(this.map.getCenter())) {
  
        this.lastValidCenter = this.map.getCenter();

      } else {

        this.map.panTo(this.lastValidCenter);
      }
    });

    google.maps.event.addListener(this.drawingManager, 'polygoncomplete', (event) => {
      if (this.isShapeDrawn) {
        alert('farm sector map already assigned');
        event.setMap(null);
        return;
      }
      console.log('POLYGON COMPLETE');
      console.log(event);
      this.shapeToStringDB(event);
      this.isShapeDrawn = true;
    });

    this.attachEventListeners();
    if (this.farmSector) {
      console.log('DRAWING FARM SECTOR POLYGON - SUCCESS');
      if (this.farmSector.dtoPolygon) {
        console.log('COORDINATES TO DRAW AT: ', this.farmSector.dtoPolygon.coordinates);
        this.drawCoordinates(this.farmSector.dtoPolygon.coordinates, this.map);
        this.isShapeDrawn = true;
      } else {
        console.log('NO POLYGON DEFINED FOR CURRENT SECTOR');
      }
    } else {
      console.log('DRAWING FARM SECTOR POLYGON - ERROR');
    }

    this.attachEventListeners();
  }

  attachEventListenersHelper(shape) {
    google.maps.event.addListener(shape.getPath(), 'insert_at', () => {
      // console.log('**INSERT EVENT*** ADDING SHAPE (new shape inserted), before: ', this.allShapes);
      this.allShapes.push(shape)
      this.shapeToStringDB(shape)

    });



    google.maps.event.addListener(shape.getPath(), 'set_at', () => {
      console.log('shape coords updated', this.getShapeCoords(shape));
      console.log('string of json: ' + JSON.stringify(this.getShapeCoords(shape)));
      let coordinatesForServer = [];

      for (let coord of this.getShapeCoords(shape)) {
        coordinatesForServer.push({
          lat: coord.latitude,
          lng: coord.longitude
        });
      }
      console.log('pushing to server: ', coordinatesForServer);
      this.service.updateSectorCoordinates(this.sectorId, coordinatesForServer).subscribe(
        rez => {
          console.log('success update coordinates: ', rez.json());
        },
        err => {
          console.log('error update coordinates: ', err.json());
        }
      );
      // alert('updated shape, go update it in your db 2');
      this.shapeToStringDB(shape);
    });
  }

  attachEventListeners(customPolygon = null) {

    if (customPolygon != null) {
      this.attachEventListenersHelper(customPolygon);
      this.allShapes.push(customPolygon);
      console.log('ALL SHAPES:', this.allShapes)
      return;

    }

    google.maps.event.addListener(this.drawingManager, 'overlaycomplete', (event) => {

      let newShape = event.overlay;
      google.maps.event.addListener(newShape, 'click', () => {
        this.setSelection(newShape);
        console.log('**click on shape');

      });




      // Polygon drawn
      if (event.type === google.maps.drawing.OverlayType.POLYGON) {
        //this is the coordinate, you can assign it to a variable or pass into another function.
        // alert(event.overlay.getPath().getArray());

        this.allShapes.push(event.overlay);
        console.log('ALL SHAPES:', this.allShapes)

      }


    });
  }

  /* --- remove shape, select shape --- */

  /**
   * Method which converts the polygon into DTO and sends it
   * via service method to the server endpoint
   * /sectors/update/{idsector}
   * @param shape 
   */
  shapeToStringDB(shape) {

    console.log('ADDED TO SHAPES: ', this.allShapes);
    let vertices = shape.getPath();

    var dtoPolygon = { coordinates: [] };
    // console.log('updating:::', coordonate.getArray());
    for (var i = 0; i < vertices.getLength(); i++) {
      var xy = vertices.getAt(i);
      console.log('point: ', xy);
      dtoPolygon.coordinates.push({
        lat: xy.lat(),
        lng: xy.lng()
      });
    }

    this.farmSector.dtoPolygon = dtoPolygon;
    this.service.updateSectorCoordinatesPolygon(this.farmSector.id, this.farmSector).subscribe(
      rez => {
        console.log('result: ', rez.json());
      },
      err => {
        console.log('ERR UPDATE: ', err);
      }
    );
    console.log('should update dtoPolygon for: ', dtoPolygon);

  }

  getShapeCoords(shape) {
    var path = shape.getPath();
    var coords = [];
    for (var i = 0; i < path.length; i++) {
      coords.push({
        latitude: path.getAt(i).lat(),
        longitude: path.getAt(i).lng()
      });
    }
    return coords;
  }

  removeShapes() {
    console.log('**REMOVING SHAPES**');
    for (let sh of this.allShapes) {
      sh.setMap(null);
    }
    this.service.updateSectorDeleteCoordinates(this.sectorId).subscribe(
      rez => {
        console.log('updated sector: ', rez.json());
        this.isShapeDrawn = false;
      },
      err => {
        console.log('eroare: ', err);
      }
    );
  }

  clearSelection() {
    if (this.selectedShape) {
      this.selectedShape.setEditable(false);
      this.selectedShape = null;
    }
  }

  setSelection(shape) {
    this.clearSelection();
    this.selectedShape = shape;
    shape.setEditable(true);
  }




}
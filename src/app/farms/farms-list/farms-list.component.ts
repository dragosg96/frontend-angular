import { Component, OnInit } from '@angular/core';
import { FarmsrvService } from '../farmsrv.service';
import { GeolocService } from '../geoloc.service';
import { Router } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';
import { LogoutdialogComponent } from '../../logoutdialog/logoutdialog.component';
import { LogoutService } from '../../logout.service';

@Component({
  selector: 'app-farms-list',
  templateUrl: './farms-list.component.html',
  styleUrls: ['./farms-list.component.css']
})
export class FarmsListComponent implements OnInit {

  farms: any[] = [];

  city : string = '';
  street : string = '';
  number : string = '';
  farmName : string = '';

  constructor(private farmService: FarmsrvService, private geoloc: GeolocService,
    private logoutService: LogoutService) { }




  loadFarms() {
    this.farmService.findAllFarms().subscribe(
      rez => {
        console.log('loaded all farms: ', rez.json());
        this.farms = rez.json();
        for (let f of this.farms) {
          console.log('FERMA === ', f);

          if (f.address) {
            this.geoloc.getLatLongFromAddress(f.address).subscribe(
              res => {
                console.log('GEOLOC RESULTS: ', res.json());


                let latLong = res.json().results[0].geometry;
                console.log('LATLONG: ', latLong);
                f.coord = latLong;
              },
              err => {
                console.log('GEOLOC ERROR: ', err);
              }
            );
          }

        }
      },
      err => {
        console.log('ERROR STATUS: ', err.status);
        if (err.status == 401) {
          
          this.logoutService.openDialog();
         
        }
      });
  }

  ngOnInit() {

    this.loadFarms();

  }

  deleteFarm(farmId: number) {
    this.farmService.deleteFarmById(farmId).subscribe(
      rez => {
        this.loadFarms();
      },
      err => {
        console.log('error: ', err);
      }
    );
  }

  savefarm(city: string, street: string, pnumber: string, farmName: string) {
  
    let jsonFarm = {
      city : city,
      street : street,
      number : pnumber,
      farmName : farmName
    };
    this.farmService.saveFarm(jsonFarm).subscribe(
      rez => {
        console.log('saved farm: ', rez.json());
        this.loadFarms();
        this.street = '';
        this.city = '';
        this.number = '';
        this.farmName = '';
      },
      err => {
        console.log('could not save farm', err)
      }
    );
  }


}


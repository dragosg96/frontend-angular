import { Injectable } from '@angular/core';
import { Http } from '@angular/http';

import {environment} from './../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class IrigationService {

  private restIrigationUrl = environment.contextPathServer + '/rest/irigation';

  appendToken(){
    return "?access_token="+localStorage.getItem('access_token');
  }

  constructor(private httpClient : Http) { }

  // http://localhost:9333/rest/irigation/bydate/2019/5/10
  // http://localhost:9333/rest/irigation/byid/46

  findOperationPropertyById(id: number){
    return this.httpClient.get(this.restIrigationUrl + '/byid/' + id  + this.appendToken());
  }

  findSectorProperties(year : number, month : number, day : number){
    return this.httpClient.get(`${this.restIrigationUrl}/bydate/${year}/${month}/${day}${this.appendToken()}` );
  }

}

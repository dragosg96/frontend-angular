import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FarmsListComponent } from './farms-list/farms-list.component';
import { FarmsDetailsComponent } from './farms-details/farms-details.component';
import { HttpModule } from '@angular/http';
import { FarmsViewComponent } from './farms-view/farms-view.component';
import { FormsModule } from '@angular/forms';
import {DpDatePickerModule} from 'ng2-date-picker';
import {NgxPaginationModule} from 'ngx-pagination';
import { FarmsSectorDetailsComponent } from './farms-sector-details/farms-sector-details.component'; // <-- import the module


import { MatDatepickerModule } from '@angular/material/datepicker';

import { MatNativeDateModule, MAT_DATE_LOCALE } from '@angular/material/core';

import { MatFormFieldModule } from '@angular/material/form-field';

import {MatInputModule} from '@angular/material';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';


import { AgmCoreModule } from '@agm/core';
import { SectorMapComponent } from './sector-map/sector-map.component';

import { RouterModule } from '@angular/router';
import { SectorComponent } from './sector/sector.component';
import { ReportsSdPropertiesComponent } from './reports-sd-properties/reports-sd-properties.component';


import { GoogleChartsModule } from 'angular-google-charts';
import { IrigationComponent } from './irigation/irigation.component';
import {MatDialogModule} from '@angular/material/dialog';

import {MatCheckboxModule} from '@angular/material/checkbox';

@NgModule({
  imports: [
    CommonModule,
    HttpModule,
    FormsModule,
    DpDatePickerModule,
    NgxPaginationModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyDyJRQ6n7i9c2ncr03yVaiDs6l45B1GTu8'
    }),
    RouterModule,
    MatDatepickerModule,
    MatFormFieldModule,
    MatNativeDateModule,
    MatInputModule,
    BrowserAnimationsModule,
    GoogleChartsModule.forRoot(),
    MatDialogModule,
    MatCheckboxModule
    
  ],
  exports: [
    FarmsListComponent, FarmsDetailsComponent, SectorMapComponent, ReportsSdPropertiesComponent,
    IrigationComponent, 
  ],
  declarations: [FarmsListComponent, FarmsDetailsComponent, 
    FarmsViewComponent, FarmsSectorDetailsComponent, SectorMapComponent, SectorComponent, ReportsSdPropertiesComponent, IrigationComponent,
    ],
  providers: [

  ],

  // providers: [
  //   { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] },
  //   { provide: MAT_DATE_FORMATS, useValue: DateFormat }
  // ]
})
export class FarmsModule { }

import { Component, OnInit, Input, Output, EventEmitter, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { FarmsrvService } from '../farmsrv.service';
import { GeolocService } from '../geoloc.service';
import { LogoutService } from '../../logout.service';

@Component({
  selector: 'app-sector',
  templateUrl: './sector.component.html',
  styleUrls: ['./sector.component.css']
})
export class SectorComponent implements OnInit, OnDestroy {

  ngOnDestroy(): void {
    console.log('sector.component.ts - ngOnDestroy')
    console.log('destroying sector component - no need execute interval')
    this.stopInterval();
  }
  idSector: number;
  sector: any = null;  // farm sector contains farm sector details (associated with operations)
  refreshSectorPropertyInterval : any = null;
  showSectorDetails : boolean = false;
  

  constructor(private route: ActivatedRoute, private service: FarmsrvService, 
    private geoloc: GeolocService
  , private logoutService : LogoutService) { }

  ngOnInit() {
    console.log('sector.component.ts - ngOnInit')
    this.route.params.subscribe(params => {
      this.idSector = params['idsector'];
      this.service.findSectorById(this.idSector).subscribe(rez => {
        this.sector = rez.json(); // load sector
        this.startInterval(); 
      },
        err => {
          console.log('error loading farm sector ')
          console.log('ERROR STATUS: ', err.status);
          if (err.status == 401) { // initiate mat dialog modal service
            // which will result in the user being redirected to the
            // login component (via router)

            this.logoutService.openDialog();


          }
        });
    });

    

  }


  formatDate(date) {
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;

    return [year, month, day].join('-');
  }

 
  startInterval(){
    this.refreshSectorPropertyInterval = setInterval(() =>{
      this.refreshAllSectorProperties();
    }, 3000);
  }

  stopInterval(){
    console.log('stopping interval - ')
    clearInterval(this.refreshSectorPropertyInterval);
  }

  refreshAllSectorProperties(){
    this.service.findAllSectorProperties(this.sector.id).subscribe(
      rez =>{
        this.sector.farmSectorProperties = rez.json();
        for(let fsp of this.sector.farmSectorProperties){
          
          fsp.dateMeasured = this.formatDate(fsp.dateMeasured);
        }
      },
      err =>{
        console.log('could not load properties for current sector: ', err);
      }
    );
  }

  evenimentSector() {
    console.log('SHOULD SEND REQUEST FOR FARM SECTOR AGAIN');
    this.service.findSectorById(this.idSector).subscribe(rez => {
      this.sector = rez.json();
    },
      err => {
        console.log('error loading farm sector ')
      });
  }


  deleteSectorProperty(prop){
    this.service.deleteSectorProperty(prop.id).subscribe(
      rez=> {
        console.log('saved new sector', rez.json());
        this.sector = rez.json();
      },
      err=>{
        console.log('error: ', err);
      }
    );
  }

  emptyAction(){
    console.log('nothing');
  }

  updateProperty(sectorid, p) {
    console.log('updating: ' + sectorid + " property: ", p);
    this.service.updateProperty(sectorid, p).subscribe(
      rez => {
        console.log('okay', rez.json());
      },
      err => {
        console.log('error:', err);
        alert('Could not update sector property');
      }
    )
  }



  saveSectorProperty(sectorId, sectorPropertyName, sectorPropertyValue) {
    console.log('saving sector property: ' + sectorId + " PNAME " + sectorPropertyName + " PVALUE " + sectorPropertyValue);
    let dtoBody = {
      farmSectorId: sectorId,
      propertyName: sectorPropertyName,
      propertyValue: sectorPropertyValue,

    };

    this.service.saveFarmSectorProperty(dtoBody).subscribe(
      rez => {
        console.log('saved dto', rez.json());

      this.service.findSectorById(this.idSector).subscribe(rez => {
        this.sector = rez.json();
        console.log('current sector: ', this.sector);
      });

      },
      err => {
        console.log(err);
        alert('Could not save Farm Sector Property');
      }
    );
  }

}

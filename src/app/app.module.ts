import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { FarmsModule } from './farms/farms.module';
import { MaterialmModule } from './materialm/materialm.module';
import { Routes, RouterModule } from '@angular/router';
import { SectorMapComponent } from './farms/sector-map/sector-map.component';
import { HomeComponent } from './home/home.component';
import { FarmsListComponent } from './farms/farms-list/farms-list.component';
import { FarmsDetailsComponent } from './farms/farms-details/farms-details.component';
import { FarmsSectorDetailsComponent } from './farms/farms-sector-details/farms-sector-details.component';
import { SectorComponent } from './farms/sector/sector.component';
import { ReportsSdPropertiesComponent } from './farms/reports-sd-properties/reports-sd-properties.component';
import { GoogleChartsModule } from 'angular-google-charts';
import { LoginComponent } from './login/login.component';
import { UserComponent } from './user/user.component';

import { FormsModule } from '@angular/forms';
import { IrigationComponent } from './farms/irigation/irigation.component';
import { LogoutdialogComponent } from './logoutdialog/logoutdialog.component';
import { MatDialogModule } from '@angular/material';
import { HttpClientModule } from '@angular/common/http';
import { SearchComponent } from './search/search.component';

import { MatAutocompleteModule } from '@angular/material/autocomplete';

import { ReactiveFormsModule } from '@angular/forms'
import { AboutComponent } from './about/about.component';

const appRoutes: Routes = [

  {
    path: 'sector-map/:idsector',
    component: SectorMapComponent,
  },
  {
    path: '',
    component: AppComponent
  },
  {
    path: 'app-farms',
    component: FarmsListComponent
  },
  {
    path: 'farms/:idfarm',
    component: FarmsDetailsComponent
  }, {
    path: 'farms/sectors/:idsector',
    component: SectorComponent
  },
  {
    path: 'reports/spropertis/:idsector',
    component: ReportsSdPropertiesComponent
  },
  { path: 'login', component: LoginComponent },
  { path: 'irigation/:id', component: IrigationComponent },
  { path : 'about', component : AboutComponent}
];

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    LoginComponent,
    UserComponent,
    LogoutdialogComponent,
    SearchComponent,
    AboutComponent
  ],
  imports: [
    BrowserModule,
    FarmsModule,
    MaterialmModule,
    RouterModule.forRoot(
      appRoutes,
      { enableTracing: true } // <-- debugging purposes only
    ),
    GoogleChartsModule.forRoot(),
    FormsModule,
    MatDialogModule,
    HttpClientModule,
    ReactiveFormsModule,
    MatAutocompleteModule

  ],
  providers: [],
  bootstrap: [HomeComponent],
  entryComponents: [
    LogoutdialogComponent
  ],
})
export class AppModule { }


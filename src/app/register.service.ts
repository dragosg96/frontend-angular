import { Injectable } from '@angular/core';
import { environment } from './../environments/environment';
import { HttpClient } from '@angular/common/http';
import { NewUserDto } from './model/newuser.dto';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class RegisterService {

  constructor(private httpClient: HttpClient) { }

  // http://localhost:9333/register/newuser
  register(registerData) : Observable<NewUserDto> {
    // let requestJson = {
    //   username: username,
    //   pssword: password,
    //   email: email
    // };

    return this.httpClient.post<NewUserDto>(environment.contextPathServer + "/register/newuser", registerData);

  }

}

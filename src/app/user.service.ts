import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from './../environments/environment'
import { Observable } from 'rxjs';
import { SecurityDto } from './model/security.dto';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  appendToken(){
    return "?access_token="+localStorage.getItem('access_token');
  }


  constructor(private http : HttpClient) { }

  getPrincipalDto() : Observable<SecurityDto>{
    return this.http.get<SecurityDto>(`${environment.contextPathServer}/rest/security/current-user${this.appendToken()}`);
  }
}

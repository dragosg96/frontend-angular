import { Component, OnInit } from '@angular/core';
import { FarmsrvService } from '../farms/farmsrv.service';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {

  allFarms: any = [];
  allFarmSectors: any = [];
  searchText: string = '';

  foundFarms: any = [];
  foundFarmSectors: any = [];

  myControl = new FormControl();
  

  constructor(private farmService: FarmsrvService) { }

  ngOnInit() {
    this.loadInformationForSearch();
  }

  loadInformationForSearch(){
    this.farmService.findAllFarms().subscribe(
      rez => {
        this.allFarms = rez.json();
        console.log('ALL FARMS: ', this.allFarms);
        

      },
      err => {
        // TODO: check if logged in
        console.log(err);
      }
    );
    this.farmService.findAllSectors().subscribe(
      rez => {
        this.allFarmSectors = rez.json();
        // this.foundFarmSectors = [...this.allFarmSectors];
        console.log('ALL FARM SECTORS: ', this.allFarmSectors);
      },
      err => {
        console.log(err);
      }
    );
  }

  doSearch(searchTerm: string) {
    if(localStorage.getItem('loggedin')){
      localStorage.removeItem('loggedin');
      this.loadInformationForSearch();

    }
    console.log('searching for: ', searchTerm);
    this.foundFarms = [...this.allFarms];
    this.foundFarmSectors = [...this.allFarmSectors];
    this.foundFarms = this.foundFarms.filter(x => x.farmName.toUpperCase().indexOf(searchTerm.toUpperCase()) != -1);
    this.foundFarmSectors = this.foundFarmSectors.filter(x => x.sectorDescription.toUpperCase().indexOf(searchTerm.toUpperCase()) != -1);

  }

}

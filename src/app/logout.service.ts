import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';
import { LogoutdialogComponent } from './logoutdialog/logoutdialog.component';

@Injectable({
  providedIn: 'root'
})
export class LogoutService {

  constructor(private router: Router,
    private dialog: MatDialog) { }

  openDialog(diagTitle = "You have been logged out", diagText = "Session ended - you will be redirected to login", okButtonText = "Proceed to login") {
    const dialogRef = this.dialog.open(LogoutdialogComponent);

    let instance = dialogRef.componentInstance;
    instance.dialogTitle = diagTitle;
    instance.dialogText = diagText;
    instance.okButtonText = okButtonText;

    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`);
      if (result == true) {
        this.router.navigate(['login']);
      }
    });
  }
}

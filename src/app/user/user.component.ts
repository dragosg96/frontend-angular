import { Component, OnInit } from '@angular/core';
import { AppService } from '../app.service';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css'],
  providers: [AppService],
})
export class UserComponent implements OnInit {

  ngOnInit() {
  }

  private url = 'http://localhost:9333/rest/users/all';  
 
  constructor(private _service:AppService) {}

  getFoo(){
      this._service.getResource(this.url)
      
  }

}

import { Component, OnInit, Input } from '@angular/core';


// Session ended - you will be redirected to login
//       <br>
//       Possible causes: token expired / server timeout

@Component({
  selector: 'app-logoutdialog',
  templateUrl: './logoutdialog.component.html',
  styleUrls: ['./logoutdialog.component.css']
})
export class LogoutdialogComponent implements OnInit {


  @Input() dialogTitle : string;
  @Input() dialogText : string;
  @Input() okButtonText : string; // Proceed to login

  constructor() { }

  ngOnInit() {
  }

}


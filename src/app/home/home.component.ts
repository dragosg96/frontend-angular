import { Component, OnInit } from '@angular/core';
import { UserService } from '../user.service';
import { SecurityDto } from '../model/security.dto';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  securityDto : SecurityDto;
  constructor(private userService : UserService) { }

  ngOnInit() {
    this.userService.getPrincipalDto().subscribe(
      rez => {
        this.securityDto = rez;
        console.log('principal = ', this.securityDto)
      }
    )
  }

}
